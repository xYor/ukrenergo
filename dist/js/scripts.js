function checker(){
  $(".js_inview").bind('inview', function (event, visible, topOrBottomOrBoth) {
    if (visible == true) {
      $(this).addClass("animation");
      numbers($(this).find("span"), $(this).find("span").data("num"));
    };
    $(this).unbind('inview');
  });
};

$.mmenu.configuration.classNames.fixedElements = {
   fixed: "Fixed"
};


// magnific
$(".feedback__to_open_popup, .feedback__to_open_popup_2").magnificPopup({
    items: {
      src: ".feedback__popup"
    },
    type: 'inline',
    closeBtnInside:false,
    showCloseBtn:false,
    mainClass: 'my-mfp-zoom-in',
    callbacks: {
      open: function(){
        wHTML.validation();
      }
    }
});
$(".slider_4__item").magnificPopup({
    gallery: {
      enabled: true
    },
    type: 'image',
    closeBtnInside:false,
    showCloseBtn:false,
    mainClass: 'my-mfp-zoom-in'
});
// $('.projects__grid__item').magnificPopup({
//     items: {
//       src: ".project__popup"
//     },
//     type: 'inline',
//     closeBtnInside:false,
//     showCloseBtn:false,
//     mainClass: 'my-mfp-zoom-in'
// });
$(document).on("click", ".projects__grid__item", function(evemt){
  

  if($(this).data("item-content") == "video"){
    var omg1 = $(this);
    var omg2 = $(this).attr("href");
    $(this).magnificPopup({
      disableOn: 700,
      // items:{
      //   src: omg2
      // },
      type: 'iframe',
      // iframe: {
      //   markup: '<div class="mfp-iframe-scaler">'+
      //           '<div class="mfp-close"></div>'+
      //           '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
      //           '</div>', 
      //   patterns: {
      //       youtube: {
      //           index: 'youtube.com/', 
      //           id: 'v=', 
      //           src: 'www.youtube.com/embed/%id%?autoplay=1' 
      //       }
      //    },
      //    srcAction: 'iframe_src', 
      // },
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
      fixedContentPos: false,
      closeBtnInside:false,
      showCloseBtn:false
    }).magnificPopup('open');
  }
  else if($(this).data("item-content") == "project"){
    $.magnificPopup.open({
      items: {
        src: ".project__popup"
      },
      type: 'inline',
      closeBtnInside:false,
      showCloseBtn:false,
      mainClass: 'my-mfp-zoom-in'
    });
  }
  else if($(this).data("item-content") == "photo"){
    $.magnificPopup.open({
      items: {
        src: ".media_popup_galry"
      },
      type: 'inline',
      closeBtnInside:false,
      showCloseBtn:false,
      mainClass: 'my-mfp-zoom-in'
    });
  }
});
$(".slider__type_2__item").magnificPopup({
    items: {
      src: ".project__popup"
    },
    type: 'inline',
    closeBtnInside:false,
    showCloseBtn:false,
    mainClass: 'my-mfp-zoom-in'
});
$('.landing_open_popup').magnificPopup({
    items: {
      src: ".landing_abus_popup"
    },
    type: 'inline',
    closeBtnInside:false,
    showCloseBtn:false,
    mainClass: 'my-mfp-zoom-in'
});
// $(".slider__type_2__item").magnificPopup({
//     gallery: {
//       enabled: true
//     },
//     type: 'image',
//     closeBtnInside:false,
//     showCloseBtn:false,
//     mainClass: 'my-mfp-zoom-in'
// });

$(document).on('click', ".btn__close", function(event) {
  $.magnificPopup.close();
});
// phones
// $('.feedback__phones_wrapper').hover(function(){
//   if($(".feedback__phones_box li").find("a").length){
//       $(".feedback__phones_box").removeClass('js__hidden');
//       $(".feedback__phones_box").addClass('js__show');
//     }
//   }, 
//   function () {
//     $(".feedback__phones_box").addClass('js__hidden');
//     $(".feedback__phones_box").removeClass('js__show');
//   }
// );

// custom selectors
function enableSelectBoxes(){
  $('.lng_drop_menu').each(function(){
    $(this).children('.lng_drop_menu__selected')
    .html($(this).children('.lng_drop_menu__options')
      .children('.lng_drop_menu__item:first').html());

    $(this).attr('value',$(this)
      .children('.lng_drop_menu__options')
        .children('.lng_drop_menu__item:first').attr('value'));

    $(this).children('.lng_drop_menu__selected').click(function(){
      if($(this).parent().children('.lng_drop_menu__options').css('top') == '-164px'){
        $(this).parent().children('.lng_drop_menu__options').css('top','82px');
        $(".lng_drop_menu").addClass("lng_drop_menu__open");
      }
      
      else{
        $(this).parent().children('.lng_drop_menu__options').css('top','-164px');
      }
    });

    $(this).find('.lng_drop_menu__item').click(function(){
      $(this).parent().css('top','-164px');

      $(this).closest('lng_drop_menu').attr('value',$(this).attr('value'));
      $(".lng_drop_menu__selected_now").removeClass("lng_drop_menu__selected_now");
      $(this).parent().siblings('.lng_drop_menu__selected').html($(this).addClass("lng_drop_menu__selected_now").html());
    });

    $(document).click(function(event){
      if(!$(event.target).closest(".lng_drop_menu").length && $(".lng_drop_menu").hasClass("lng_drop_menu__open")){
        $('.lng_drop_menu__options').css('top','-164px');
      }
    });
  });       
}
$(document).on('click', ".hamburger", function(event) {
  $(".hamburger-box").addClass('is-active');
  $(".hamburger").addClass('is_api');
});

$(document).ready(function() {
  // $(".page_header").removeClass("Fixed");
  // if($(window).width() <= 1024){
  //   $(".page_header").addClass("Fixed");
  //   console.log("test"); 
  // }
  initpopslide();
  enableSelectBoxes();
  checker();
  init_slide_2(".slider__type_2__window", ".slider__type_2__item");


  if ($("#gmap").length) {
    var map;
    
    // $('#gmap').each(function(index, el) {
    //   var map_x = $("#gmap").data("map-x");
    //   var map_y = $("#gmap").data("map-y");
    //   var zoom = 17;
    //   loadGoogleMap(el,map_x,map_y,zoom);
    // });
    // function loadGoogleMap(mapelement,x,y,zoom) {
    function loadGoogleMap() {
      // map = new google.maps.Map(mapelement, {
      var map_x = Number($("#gmap").data("map-x"));
      var map_y = Number($("#gmap").data("map-y"));
      map = new google.maps.Map(document.getElementById("gmap"), {
        zoom: 17,
        center: {
          lat: map_x,
          lng: map_y
        },
        zoomControl: false,
        streetViewControl: false,
        scrollwheel: false,
        mapTypeControl: false
      });
      var marker = new google.maps.Marker({
        position: {
          lat: map_x,
          lng: map_y
        },
        map: map
      });
      var mapTimer = null;
      $(window).resize(function(){
        clearTimeout(mapTimer);
        mapTimer = setTimeout(function() {
          map.setCenter({
            lat: map_x,
            lng: map_y
          });
        }, 100);
      });
    }
    loadGoogleMap();
  }
  // MMENU
  $(function() {
    // $(".page_header__nav ul").clone().appendTo('#m_menu');
    var lng = $(".lng_drop_menu__options").clone();
    var soc = $(".socs_block").clone();
    // var phones = $(".feedback__phones_box").clone();
    $('#m_menu').mmenu({
      navbars: [
        {
          position: "top",
          content: lng.html()
        },
        // {
        //   title: "",
        //   position: "bottom"
        //   // content: phones.html()
        // },
        {
          position: "bottom",
          content: soc.html()
        }
      ],
      // "autoHeight": true,
      navbar: {
        title: ""
      },
      "extensions": [
         "pagedim-black",
         "effect-menu-fade"
      ],
      // "offCanvas": {
      //       "position": "top",
      //       "zposition": "front"
      //    }
    });
    var api = $("#m_menu").data("mmenu");

    api.bind('opened', function () {
        $(".mmenufixedcrox").fadeIn(1400);
    });
    function on_anchor(anchor){
      // klik na anchor
      $(anchor).on('click', function(){
        // predotvratit event
        event.preventDefault();
        var omg = $(this).attr("data-anchor");
        if ($("#m_menu").hasClass("mm-opened")){
          // zakrit ego
          api.close();
          // posle zakritia otrabotaet...
          api.bind('closed', function () {
            // skroll k anchoru
            $('html, body').stop().animate({
		      scrollTop: $('.js_anchor_block[data-anchor="'+omg+'"]').offset().top
		    }, 600,function(){
		      $('body,html').css('overflow', 'auto');
		    });
          });
        }
        else{
          // skroll k anchoru
          $('html, body').stop().animate({
		      scrollTop: $('.js_anchor_block[data-anchor="'+omg+'"]').offset().top
		    }, 600,function(){
		      $('body,html').css('overflow', 'auto');
		    });
        }
    });
  };
  // aktiviruet funkciu skrolla
  on_anchor(".js-anchor_menu");
    $(".hamburger").on('click', function(event){
    if($(".hamburger").hasClass('is_api')){
      $(".mmenufixedcrox").fadeOut(200);
        api.close();
      }
    });
    api.bind("closing", function(event){
      $(".mmenufixedcrox").fadeOut(0);
    });
    api.bind('closed', function(event){
      $(".is-active").removeClass('is-active');
      $(".hamburger").removeClass('is_api');
      
      // $("body").removeClass('js-mmenu_opened');
    });
    // api.bind( "openPanel", function( $panel ) {
    //   $("body").addClass('js-mmenu_opened');
    // });
  });
  $(window).on("wheel", function(event) {
      $('body,html').stop();
  });

  
  

  // slider 3
  $('.slider__type_3').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 3,
    centerMode: true,
    arrows: false,
    variableWidth: true,
    // responsive: [
    // {
    //   breakpoint: 480,
    //   settings: {
    //     arrows: false,
    //     centerMode: true,
    //     centerPadding: '40px',
    //     slidesToShow: 1
    //   }
    // }
  // ]
  });

  $('.slider_4').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    centerMode: false,
    arrows: false,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4
      }
    },
    {
      breakpoint: 875,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 625,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 420,
      settings: {
        slidesToShow: 1
      }
    },
  ]
  });
  // popup_slider
  $('.media_galry__window').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    variableWidth: true,
    centerMode: true,
    responsive: [
    // {
    //   breakpoint: 1024,
    //   settings: {
    //     centerMode: true,
    //     slidesToShow: 3,
    //     variableWidth: false
    //   }
    // },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
      }
    },
    // {
    //   breakpoint: 480,
    //   settings: {
    //     centerMode: true,
    //     slidesToShow: 1
    //   }
    // }
  ]
  });
});
$(".slider__type_3__arrow_next--media_exp").on("click", function(event){
  $('.media_galry__window').slick('slickNext');
});
$(".slider__type_3__arrow_prev--media_exp").on("click", function(event){
  $('.media_galry__window').slick('slickPrev');
});
$(".slider__type_3__arrow_next").on("click", function(event){
  $('.slider__type_3').slick('slickNext');
});
$(".slider__type_3__arrow_prev").on("click", function(event){
  $('.slider__type_3').slick('slickPrev');
});
$(".slider__type_4__arrow_next").on("click", function(event){
  $('.slider_4').slick('slickNext');
});
$(".slider__type_4__arrow_prev").on("click", function(event){
  $('.slider_4').slick('slickPrev');
});
// $('.testt').magnificPopup({
//   disableOn: 700,
//   type: 'iframe',
//   mainClass: 'mfp-fade',
//   removalDelay: 160,
//   preloader: false,

//   fixedContentPos: false
// });


$(".media_galry__item__wrapper").on("click", function(event){
  $('.media_galry__active__image').find('.media_galry__item').eq(0).remove(); 
  $(this).find(".media_galry__item").clone().appendTo('.media_galry__active__image');
});
function galinit(){
  $('.media_galry__item__wrapper').find('.media_galry__item').eq(0).clone().appendTo('.media_galry__active__image');
}
galinit();

function numbers(num, final) {
  var number = 0;
  var time = 2000;
  var interval = time / final;
  // var increment = final / interval;
  var increment = 1;
  if (interval < 8) {
    increment = 2;
  }
  var timerId = setInterval(function() {
    $(num).text(parseInt(number));
    if (number >= final) {
      clearInterval(timerId);
    }
    number += Math.round(increment);
  }, interval);
}



$(document).on("click",".js-to_top", function (event) {
  event.preventDefault();
  $('body,html').animate({
    scrollTop: 0
  }, 800, function(){
  });
});

// settings
var repeat = false;
var repeat_speed = 5000;

var stop_carousel_on_hover = true;

var carousel_speed = 500;

var carousel_main_block = ".project__popup__galry__img";
var carousel_item = ".project__popup__galry__img .project__popup__galry__img__item";


// ////////////////////////////////////////////////////////////////// 
// // slider main functions
// //////////////////////////////////////////////////////////////////
function totop(parent, children, exp){
  if(exp == true){
    $(".project__popup__galry__img__item--active__img")
      .removeClass('project__popup__galry__img__item--active__img');
  }
  if(!$(parent).hasClass('sliding')){
    $(parent).addClass('sliding');
    var block_height = $(carousel_item).outerHeight() + 10;
    $(children).eq(-1).clone().appendTo($(parent));
    $(children).eq(-1).insertBefore($(children).eq(0));
    $(parent).css({
      "margin-top": "-" + block_height + "px"
    }); 
    $(parent).stop().animate({
      marginTop: 0 +"px"
    }, carousel_speed, function(event){
      $(children + ":last-child").remove(); 
      $(parent).removeClass('sliding');
    });
    if(exp == true){
      $(children).eq(0).addClass('project__popup__galry__img__item--active__img')
      $(children).eq(0)
        .clone().appendTo(".project__popup__galry__img__window");
        $(".project__popup__galry__img__window div").eq(0).remove(); 
    }
  }
  else{
    return;
  }
}
function tobottom(parent, children, exp){
  if(exp == true){
    $(".project__popup__galry__img__item--active__img")
      .removeClass('project__popup__galry__img__item--active__img');
  }
  if(!$(parent).hasClass('sliding')){
    $(parent).addClass('sliding');
    var block_height = $(carousel_item).outerHeight() + 10;
    $(children).eq(0).clone().appendTo($(parent));
    $(parent).stop().animate({
      marginTop: "-"+ block_height +"px"
    }, carousel_speed, function(event){
      $(children).eq(0).remove(); 
      if(exp == true){
        $(children).eq(0).addClass('project__popup__galry__img__item--active__img');
        $(children).eq(0)
          .clone().appendTo(".project__popup__galry__img__window");
        $(".project__popup__galry__img__window div").eq(0).remove(); 
      }
      $(parent).css({
        "margin-top":"0px"
      });
      $(parent).removeClass('sliding');
    });
  }
  else{
    return;
  }
}
// apend slide
$(document).on("click",".project__popup__galry__img__item", function(){
  if(!$(this).parent().hasClass('project__popup__galry__img__window')){
    $(".project__popup__galry__img__window").children().remove();
    $(this).clone().appendTo('.project__popup__galry__img__window');
    $(".project__popup__galry__img__item--active__img")
      .removeClass('project__popup__galry__img__item--active__img');
    $(this).addClass('project__popup__galry__img__item--active__img');
    $(".project__popup__galry__img__window").children()
    .removeClass('project__popup__galry__img__item');
  }
});
$(document).on("click",".project__popup__galry__arrow_toleft", function(){
  totop(carousel_main_block, carousel_item, true);
});
$(document).on("click",".project__popup__galry__arrow_toright", function(){
  tobottom(carousel_main_block, carousel_item, true);
});
function initpopslide(){
  $(".project__popup__galry__img__item--active__img").clone()
    .appendTo('.project__popup__galry__img__window');
  $(".project__popup__galry__img__window").children()
    .removeClass('project__popup__galry__img__item');
}
if(repeat == true){
  setInterval(function(event){
    if (stop_carousel_on_hover == true){
      if (!$(".slider__type_1__wrapper").is(':hover')){
        tobottom(carousel_main_block, carousel_item);
      }
    }
    else{
      tobottom(carousel_main_block, carousel_item);
    }
  },repeat_speed);
}
// // Arrows
$(document).on("click", ".project__popup__galry__arrow_top", function() {
  totop(carousel_main_block, carousel_item, false);
});
$(document).on("click", ".project__popup__galry__arrow_btm", function() { 
  tobottom(carousel_main_block, carousel_item, false);
});

$('.fade_test .slider__type_1__slider_items').slick({
  infinite: true,
  speed: 500,
  fade: true,
  cssEase: 'linear',
  swipe: false,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 4000,
  pauseOnHover: false
});
$(".slider__type_1__arrow_top").on("click", function(event){
  $('.fade_test .slider__type_1__slider_items').slick('slickNext');
});
$(".slider__type_1__arrow_bottom").on("click", function(event){
  $('.fade_test .slider__type_1__slider_items').slick('slickPrev');
});



// //////////////////////////////////////////
// // EXTENDS
// //////////////////////////////////////////


// slider_2
function init_slide_2(parent, children){
  $(children).eq(-1).clone().appendTo($(parent));
  $(children).eq(-1).insertBefore($(children).eq(0));
  $(children + ":last-child").remove(); 
};

function toleft(parent, children){
  if(!$(parent).hasClass('sliding')){
    var block_width = $(children).outerWidth() - 195;
    $(parent).addClass('sliding');
    $(children).eq(-1).clone().appendTo($(parent));
    $(children).eq(-1).insertBefore($(children).eq(0));
    $(children).eq(1).addClass('slider__type_2__active')
      .addClass('js__animation__fadeIn');
    $(children).eq(2).removeClass('slider__type_2__active');
    $(parent).css({
      "margin-left": "-" + block_width + "px"
    }); 
    $(parent).stop().animate({
      marginLeft: 0 +"px"
    }, carousel_speed, function(event){
      $(children + ":last-child").remove(); 
      $(parent).removeClass('sliding');
    }); 
  }
}
function toright(parent, children){
  if(!$(parent).hasClass('sliding')){
    var block_width = $(children).outerWidth() + 30;
    $(parent).addClass('sliding');
    $(children).eq(0).clone().appendTo($(parent));
    $(children).eq(2).addClass('slider__type_2__active');
    $(children).eq(1).removeClass('slider__type_2__active');
    $(parent).stop().animate({
      marginLeft: "-"+ block_width +"px"
    }, carousel_speed, function(event){
      $(children).eq(0).remove(); 
      $(parent).removeClass('sliding');
      $(parent).css({
        "margin-left":"0px"
      }); 
    });
  }
}

// Arrows
$(document).on("click", ".slider__type_2__arrow_toleft", function() {
  toleft(".slider__type_2__window", ".slider__type_2__item");
});
$(document).on("click", ".slider__type_2__arrow_toright", function() { 
  toright(".slider__type_2__window", ".slider__type_2__item");
});
// SWIPE
var start;
var final;
var touchOffsetX;
if($(".slider__type_2").length){
  (document).querySelector(".slider__type_2").addEventListener('touchstart', function(event){
    start = event.changedTouches[0];
    touchOffsetX = start.pageX - start.target.offsetLeft;
  });
  (document).querySelector(".slider__type_2").addEventListener('touchmove', function(event) {
    if (event.targetTouches.length == 1) {
      var touch = event.targetTouches[0];
      if(touch.pageX - touchOffsetX < 45 && touch.pageX - touchOffsetX > -45){
        (document).querySelector(".slider__type_2").style.left = touch.pageX - touchOffsetX + 'px';
      }
    }
  });
  (document).querySelector(".slider__type_2").addEventListener('touchend', function(event){
    final=event.changedTouches[0];
    (document).querySelector(".slider__type_2").style.left = 0;
    var x = Math.abs(start.pageX - final.pageX);
    if (x > 40){
      if (final.pageX < start.pageX){
        toright(".slider__type_2__window", ".slider__type_2__item");
      }
      else{
        toleft(".slider__type_2__window", ".slider__type_2__item");
      }
    }
  });
}

// определить девайс
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i
    .test(navigator.userAgent) ) {
  $(".direct__block__blocks_wrapper").addClass('js_iftouch');
  $(".arrow_to_bot").addClass("js__hidden");
}
