var gulp = require('gulp');
var sass = require('gulp-sass');


gulp.task('style', function(){
  return gulp.src('src/sass/style.scss')
  	.pipe(sass().on('error', sass.logError))
  	.pipe(gulp.dest('dist/css')
   );
});

gulp.task('html', function(){
  return gulp.src('src/*.html')
  	.pipe(gulp.dest('dist/')
  );
});

gulp.task('watch', function(){
  gulp.watch('src/sass/style.scss', ['style']);
  gulp.watch('src/*.html', ['html']);
});

gulp.task('default', ['style', 'html', 'watch']);